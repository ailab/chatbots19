import logging
from pprint import pprint
from collections import namedtuple

logger = logging.getLogger(__name__)


tsv3_escape_strings = '\,[,],|,_,->,;,\t,\n,*'.split(',')


def tsv3_escape(value:str):
    for subst in tsv3_escape_strings:
        value = value.replace(subst, '\\' + subst)
    return value


def get_base_cols(sentences):
    res = []
    for s_id, sentence in enumerate(sentences, start=1):
        sent_rows = []
        for t_id, t in enumerate(sentence['tokens'], start=1):
            t_text = tsv3_escape(t['form'])
            t_start = t['start']
            t_end = t['end']
            line = '{}-{}\t{}-{}\t{}'.format(s_id, t_id, t_start, t_end, t_text)
            sent_rows.append(line)
        res.append(sent_rows)
    return res


def get_ner_col(sentences):
    res = []
    next_id = 1
    for sentence in sentences:
        ner_labels = [[] for _ in sentence['tokens']]

        for e in sentence.get('ner', []):
            e_label = e['label']
            e_start = e['start']
            e_end = e['end']
            e_id = next_id
            next_id += 1

            for i in range(e_start, e_end):
                ner_labels[i].append('{}[{}]'.format(e_label, e_id))

        ner_labels = ['|'.join(e) or '_' for e in ner_labels]
        res.append(ner_labels)
    return res


def get_pos_col(sentences):
    res = []
    for sentence in sentences:
        res.append([t['tag'] for t in sentence['tokens']])
    return res


def get_lemma_col(sentences):
    res = []
    for sentence in sentences:
        res.append([tsv3_escape(t['lemma']) for t in sentence['tokens']])
    return res


def get_ud_col(sentences):
    res = []
    for sentence_i, sentence in enumerate(sentences, start=1):
        res.append(['{}\t{}\t{}-{}'.format(t['deprel'], 'basic', sentence_i, t['parent'] or t_i) for t_i, t in enumerate(sentence['tokens'], start=1)])
    return res


def get_id_col(sentences):
    res = []
    for sentence_i, sentence in enumerate(sentences, start=1):
        res.append([f'{t["id"]}' for t in sentence['tokens']])
    return res


TsvLayer = namedtuple('TsvLayer', ['map', 'header'])


class Layer:
    POS = TsvLayer(get_pos_col, '#T_SP=de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS|PosValue')
    LEMMA = TsvLayer(get_lemma_col, '#T_SP=de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Lemma|value')
    NER = TsvLayer(get_ner_col, '#T_SP=de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity|value')
    UD = TsvLayer(get_ud_col, '#T_RL=de.tudarmstadt.ukp.dkpro.core.api.syntax.type.dependency.Dependency|DependencyType|flavor|BT_de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS')
    ID = TsvLayer(get_id_col, '#T_SP=webanno.custom.ID|value')


def get_tsv(doc, *layers: TsvLayer, to_file=None):
    sentences = doc['sentences']
    text = doc['text']

    res = ['#FORMAT=WebAnno TSV 3.2\n']
    for layer in layers:
        res.append(layer.header)
        res.append('\n')
    res.append('\n')

    texts = [text[s['tokens'][0]['start']:s['tokens'][-1]['end']].replace('\n', '\\n') for s in sentences]
    cols = [get_base_cols(sentences)]
    for layer in layers:
        cols.append(layer.map(sentences))
    lines = [['\t'.join(cols) for cols in zip(*sent_cols)] for sent_cols in zip(*cols)]
    for s_text, s_lines in zip(texts, lines):
        res.append('\n#Text={}\n'.format(s_text))
        res.append('\n'.join(s_lines))
        res.append('\n')

    res = ''.join(res)

    if to_file:
        with open(to_file, 'w') as f:
            f.write(res)

    return res


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(name)s : %(levelname)s : %(message)s', level=logging.INFO)

    from utils.nlpclient import NlpClient, retokenize
    nlp = NlpClient()

    text = """Re: Par LMT rēķina apmaksu

Labdien!
Ludzu pagarināt rēķina atmaksas terminu līdz 15.03.2018.

Alise 
 projektēšanas birojs


On 27 Feb 2018, at 12:11, info@lmt.lv wrote:

Šodien Jānis Bērziņš.
"""

    doc = nlp.process_text(text, ['morpho', retokenize, 'parser', 'ner'])
    pprint(doc)
    get_tsv(doc, Layer.LEMMA, Layer.POS, Layer.UD, Layer.NER, to_file='./test.tsv')
