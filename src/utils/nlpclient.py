import logging
import requests

logger = logging.getLogger(__name__)


class NlpClient:
    def __init__(self, api_url='http://nlp.ailab.lv/api/nlp'):
        self.api_url = api_url

    def _process(self, doc, steps):
        r = requests.post(self.api_url, json={'data': doc, 'steps': steps})
        res = r.json()
        logger.debug('returned data from nlp api {}'.format(res))
        return res['data']

    def process(self, doc, steps):
        if isinstance(doc, str):
            doc = {'text': doc}
        cur_steps = []
        for step in steps:
            if isinstance(step, str):
                cur_steps.append(step)
            else:
                if cur_steps:
                    doc = self._process(doc, cur_steps)
                    cur_steps = []
                doc = step(doc)
        if cur_steps:
            doc = self._process(doc, cur_steps)
        return doc

    def process_text(self, text, steps=None):
        return self.process({'text': text}, steps=steps)


def retokenize(doc):
    text: str = doc['text']
    tokens = []
    pos = 0
    for s in doc['sentences']:
        for t in s['tokens']:
            start = text.find(t['form'], pos)
            assert start >= 0
            end = start + len(t['form'])
            assert text[start:end] == t['form']
            pos = end
            t['start'] = start
            t['end'] = end
    return doc


def retokenize_by_newline(doc):
    text: str = doc['text']
    tokens = []
    for s in doc['sentences']:
        tokens.extend(s['tokens'])
    sentences = []
    sentence_tokens = []
    pos = 0
    for t in tokens:
        start = text.find(t['form'], pos)
        assert start >= 0
        end = start + len(t['form'])
        assert text[start:end] == t['form']
        if '\n' in text[pos:start]:
            if sentence_tokens:
                sentences.append({'tokens': sentence_tokens})
                sentence_tokens = []
        pos = end
        t['start'] = start
        t['end'] = end

        sentence_tokens.append(t)

    if sentence_tokens:
        sentences.append({'tokens': sentence_tokens})
    doc['sentences'] = sentences
    return doc
