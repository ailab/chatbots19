import json
import logging
import os
import re
import requests

logger = logging.getLogger('nlp_client')


class NlpClient:
    def __init__(self, api_url='http://nlp.ailab.lv/api/nlp'):
        self.api_url = api_url

    def process_text(self, text, steps=None):
        steps = steps or ['tokenizer', 'morpho', 'parser', 'ner']
        r = requests.post(self.api_url, json={'data': {'text': text}, 'steps': steps})
        data = r.json()['data']
        data['text'] = text
        return data


def tokens_to_conll(tokens, with_spaces=True, metadata: dict=None, with_features=True):
    lines = []
    for field, value in (metadata or {}).items():
        lines.append(f'# {field} = {value}')
    for t in tokens:
        fields = [
            t.get('index', '_'),
            t.get('form', '_') if with_spaces else t.get('form', '_').replace(' ', '_'),
            t.get('lemma', '_') if with_spaces else t.get('lemma', '_').replace(' ', '_'),
            t.get('upos', '_'),
            t.get('tag', '_'),
            t.get('features', '_').replace('\n', '\\n').replace('\t', '\\t') if with_features else '_',
            t.get('parent', '_'),
            t.get('deprel', '_'),
            '_',
            '_'
        ]
        lines.append('\t'.join([str(f) for f in fields]))
    return '\n'.join(lines)


def get_sentence_text_span(document_text: str, sentence_tokens, start=0):
    start_position = None
    end_position = start
    for i, t in enumerate(sentence_tokens):
        end_position = document_text.find(t, end_position) + len(t)
        if i == 0:
            start_position = end_position - len(t)
    return start_position, end_position


def convert_forum_data_to_conllu(threads, output_dir):
    TAG_RE = re.compile(r'<[^>]+>')
    os.makedirs(output_dir, exist_ok=True)

    nlp_client = NlpClient()

    for thread in threads:
        thread_id = thread['id']
        label_id = thread['label']
        thread_conll = []

        for message_idx, message in enumerate(thread['messages'], start=1):
            message_id = message['id']
            message_text_orig = message['text']
            message_text = TAG_RE.sub('', message_text_orig)
            # print('---', message_text)
            sentences = nlp_client.process_text(message_text, steps=['morpho', 'parser'])['sentences']
            offset = 0
            for sentence_idx, sentence in enumerate(sentences, start=1):
                sentence_start_offset, sentence_end_offset = get_sentence_text_span(message_text, (t['form'] for t in sentence['tokens']), offset)
                sentence_text = message_text[sentence_start_offset:sentence_end_offset].replace('\n', '\\n')
                offset = sentence_end_offset
                metadata = {}
                if sentence_idx == 1:
                    if message_idx == 1:
                        metadata['newdoc id'] = thread_id
                        metadata['newdoc label'] = label_id
                    metadata['newpar id'] = f'{thread_id}-{message_id}'
                metadata['sent_id'] = f'{thread_id}-{message_id}-s{sentence_idx}'
                metadata['text'] = sentence_text

                # webanno conllu format import does not allow non_ud features, so ignore them
                sentence_conll = tokens_to_conll(sentence['tokens'], metadata=metadata, with_features=False)
                # print(sentence_conll)
                thread_conll.append(sentence_conll)

        with open(os.path.join(output_dir, f'{label_id}_{thread_id}.conllu'), 'w') as f:
            f.write('\n\n'.join(thread_conll))
            f.write('\n')


if __name__ == '__main__':
    convert_forum_data_to_conllu(json.load(open('../data/ul_forum_output.json')), '../data/ul_forum_ud')
