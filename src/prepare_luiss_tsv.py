import json
import logging
import os
import re
from pprint import pprint

from utils.tsv3 import get_tsv, Layer
from utils.nlpclient import NlpClient, retokenize


def convert_forum_data_to_tsv(threads, output_dir):
    TAG_RE = re.compile(r'<[^>]+>')
    os.makedirs(output_dir, exist_ok=True)

    nlp_client = NlpClient()

    for thread in threads:
        thread_id = thread['id']
        label_id = thread['label']
        thread_conll = []
        print('..', thread_id)

        text = ''
        sentences = []
        message_offsets = [0]
        message_ids = [e['id'] for e in thread['messages']]

        for message_idx, message in enumerate(thread['messages'], start=1):
            message_text_orig = message['text']
            message_text = TAG_RE.sub('', message_text_orig)

            text += message_text
            text += '\n\n'
            message_offsets.append(len(text))
            sentences += nlp_client.process_text(message_text, steps=['tokenizer', 'morpho', 'parser', 'ner'])['sentences']

        doc = {'text': text, 'sentences': sentences}
        doc = retokenize(doc)
        for sentence in doc['sentences']:
            for token in sentence['tokens']:
                token['id'] = f'{label_id}_{thread_id}_{[mid for mid, mo in zip(message_ids, message_offsets) if mo <= token["start"]][-1]}'
        # pprint(doc)
        get_tsv(doc, Layer.LEMMA, Layer.POS, Layer.UD, Layer.NER, Layer.ID, to_file=os.path.join(output_dir, f'{label_id}_{thread_id}.tsv'))


convert_forum_data_to_tsv(json.load(open('../data/luiss.json'))[:], '../data/luiss')
