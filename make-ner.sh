#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")/ner"

IMG=registry.gitlab.com/ailab/chatbots19/ner

if [ $# -eq 0 ]; then
    echo "Build/push backend"
    GIT_REV=$(git rev-parse --short=8 HEAD)
    echo "Tag (default=$GIT_REV):"
    read TAG
    TAG=${TAG:-$GIT_REV}
    echo  "TAG='$TAG'"
    docker build -t $IMG:$TAG . && docker push $IMG:$TAG
fi
