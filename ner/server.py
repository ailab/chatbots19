import os
import logging
from flask import Flask, jsonify, request, send_from_directory, redirect
from flask_cors import CORS

from ner import TaggerPredictor

from dotenv import load_dotenv

load_dotenv()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
logger = logging.getLogger('api')
logger.debug(os.environ)

PORT = os.getenv('PORT', '9501')
HOST = os.getenv('HOST', '0.0.0.0')
NER_MODEL = os.getenv('NER_MODEL', '/model/model.tar.gz')

app = Flask(__name__)
CORS(app)
app.ner = TaggerPredictor.from_path(NER_MODEL, 'tagger')


@app.route('/')
def index():
    return redirect('/ner')


@app.route('/api/ner', methods=['GET', 'POST'])
def api_ner():
    req = request.get_json(force=True, silent=True) or request.values
    text = req['text'] or ''
    try:
        entities = app.ner.get_entities(text)
        return jsonify(entities)
    except Exception as e:
        logger.exception('Exception on ner {}... {}'.format(repr(text), e))
        return jsonify({'error': str(e)}), 500


@app.route('/ner')
def ner_demo():
    return send_from_directory(os.path.abspath(os.path.join(os.path.dirname(__file__))), 'demo.html')


if __name__ == '__main__':
    print('RUN API %r %r' % (HOST, PORT))
    app.run(host=HOST, port=int(PORT), debug=False, threaded=False)
