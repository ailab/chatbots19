import logging
import re
from typing import Tuple, List
import urllib.parse

from allennlp.data.tokenizers import Token
from allennlp.predictors import Predictor
import requests

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def substitute_text(text, substitutions: List[Tuple[int, int, str]]):
    substitutions.sort(key=lambda x: x[0], reverse=True)
    substituted_text = text
    for sub_start, sub_end, sub_text in substitutions:
        substituted_text = text[:sub_start] + sub_text + substituted_text[sub_end:]
    return substituted_text


def normalize_entity_category(category):
    """
    Normalize named entity category for inflection (person|loc|org). Otherwise it default to value "other" in morphology endpoint.
    """
    if category == 'organization':
        category = 'org'
    if category == 'location' or category == 'GPE':
        category = 'loc'
    return category


def normalize_phrase(text, category): # person|loc|org
    return requests.get(f'http://api.tezaurs.lv:8182/normalize_phrase/{urllib.parse.quote(text)}?category={urllib.parse.quote(category or "")}').text


def inflect_phrase(text, category): # person|loc|org
    return requests.get(f'http://api.tezaurs.lv:8182/inflect_phrase/{urllib.parse.quote(text)}?category={urllib.parse.quote(category or "")}').json()


def normalize_named_entities(entities):
    for entity in entities:
        entity['text_normalized'] = normalize_phrase(entity['text'], normalize_entity_category(entity['label']))
    return entities


class WsTokenizer:
    RE_WORD_PUNKT_TOKENIZER = re.compile(r'\n|\w+|[^\w\s]+')

    def __init__(self, min_sentence_newlines=1):
        self.min_sentence_newlines = min_sentence_newlines

    def _spans(self, s):
        return [(s[m.start():m.end()], m.start(), m.end()) for m in WsTokenizer.RE_WORD_PUNKT_TOKENIZER.finditer(s)]

    def words(self, s):
        return [w for w, _, _ in self._spans(s) if w != '\n']

    def sentences(self, text):
        return [text[sent[0][1]:sent[-1][2]] for sent in self.tokenize(text)]

    def tokenize(self, s):
        sentences = []
        sentence = []
        newline_count = 0
        for w, s, e in self._spans(s):
            if w == '\n':
                newline_count += 1
            else:
                newline_count = 0
                sentence.append((w, s, e))

            if newline_count > self.min_sentence_newlines and sentence:
                sentences.append(sentence)
                sentence = []
        if sentence:
            sentences.append(sentence)
        return sentences


@Predictor.register('tagger')
class TaggerPredictor(Predictor):
    def __init__(self, model, dataset_reader):
        super().__init__(model, dataset_reader)
        self._tokenizer = WsTokenizer()

    def get_spans(self, text, labels, start_offsets, end_offsets):
        spans = []
        prev_label = None
        prev_start = -1
        prev_end = -1
        for l, s, e in zip(labels, start_offsets, end_offsets):
            prefix, label, *_ = l.split('-', 1) + [None]
            if prefix == 'B' or prefix == 'U' or (prev_label and prev_label != label):
                # new tag starts
                if prev_label:
                    spans.append({'text': text[prev_start:prev_end], 'label': prev_label, 'start': prev_start, 'end': prev_end})
                prev_label = label
                prev_start = s
                prev_end = e
            elif label is None:
                # outside
                if prev_label:
                    spans.append({'text': text[prev_start:prev_end], 'label': prev_label, 'start': prev_start, 'end': prev_end})
                prev_label = None
                prev_start = -1
                prev_end = -1
            else:
                # tag continues
                prev_end = e
        if prev_label:
            spans.append({'text': text[prev_start:prev_end], 'label': prev_label, 'start': prev_start, 'end': prev_end})
        return spans

    def get_entities(self, text):
        res = []
        sentences = self._tokenizer.tokenize(text)
        instances = [self._dataset_reader.text_to_instance([Token(t[0]) for t in s]) for s in sentences]
        output_instances = self.predict_batch_instance(instances)
        for s, oi in zip(sentences, output_instances):
            res += self.get_spans(text, oi.get('tags'), [x[1] for x in s], [x[2] for x in s])
        return normalize_named_entities(res)

    def anonymize(self, text, ignore_labels: List[str] = None):
        ignore_labels = ignore_labels or []
        entities = self.get_entities(text)
        substitutions = []
        for e in entities:
            e_start = e['start']
            e_end = e['end']
            e_label = e['label']
            if e_label in ignore_labels: continue
            substitutions.append((e_start, e_end, '$$' + e_label + '$$'))
        return substitute_text(text, substitutions)

    def predict(self, sentence: str):
        return self.predict_json({"sentence": sentence})

    def _json_to_instance(self, json_dict):
        sentence = json_dict["sentence"]
        tokens = [Token(w) for w in self._tokenizer.words(sentence)]
        return self._dataset_reader.text_to_instance(tokens)


if __name__ == '__main__':
    print(normalize_phrase('Džeimmas Lapiņas', 'person'))
    print(normalize_phrase('Džeimmas Lapiņas', 'other'))
    print(normalize_phrase('Latvijas Gāzi', 'org'))
    print(normalize_phrase('Latvijas Gāzi', 'person'))

    p = TaggerPredictor.from_path('/Users/arturs/Documents/aic/chatbots19/models/ner_luiss_m1/model.tar.gz', 'tagger')
    print(p.get_entities("""Modrim Lācim\n\nNo Rasas Jankovskas"""))
